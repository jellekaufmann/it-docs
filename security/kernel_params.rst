.. index:: hardening; kernel, hardening; boot parameters

####################################
Kernel Hardening via Boot Parameters
####################################

Set Boot Parameters via Grub
============================

Example config for the Grub boot loader (``/etc/default/grub.d/50_hardening.cfg``)::

    GRUB_CMDLINE_LINUX_DEFAULT="init_on_alloc=1 nosmt=force vsyscall=none"

Then update the config::

    $ update-grub

.. tip::

    On Debian, the package `hardening-runtime`_ can be installed which
    configures many of this parameters for you. Look at
    ``/etc/default/grub.d/01_hardening.cfg`` after installing it.


Boot Parameters
===============

*init_on_alloc*
---------------

Zero newly allocated pages::

    init_on_alloc=1

See:

* init_on_allow= in `The kernel’s command-line parameters`_
* `security things in Linux v5.3`_

*init_on_free*
--------------

Zero free pages::

    init_on_free=1

.. warning::

   Stick with only `init_on_alloc`_ if performance is a
   concern.

See:

* init_on_allow= in `The kernel’s command-line parameters`_
* `security things in Linux v5.3`_


*nosmt*
-------

Force-disable symmetric multithreading (AKA hyperthreading)::

    nosmt=force

See nosmt= in `The kernel’s command-line parameters`_


*page_alloc.shuffle*
--------------------

Tell page allocator to shuffle free list unconditionally::

    page_alloc.shuffle=1

See:

* page_alloc.shuffle= in `The kernel’s command-line parameters`_
* `security things in Linux v5.2`_


*pti*
-----

Turn on :abbr:`PTI (Page Table Isolation)` unconditionally::

    pti=on

See:

* pti= in `The kernel’s command-line parameters`_
* `Page Table Isolation (kernel docs)`_


*random.trust_cpu*
------------------

Do not trust CPU as a source of randomness::

    random.trust_cpu=off

Concerns about `backdoors`_ have been raised and a series of bugs
[`1`_,\ `2`_,\ `3`_] have been uncovered.


.. _backdoors: https://lkml.org/lkml/2018/7/17/1279
.. _1: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=c49a0a80137c7ca7d6ced4c812c9e07a949f6f24
.. _2: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=7879fc4bdc7506d37bd67b6fc29442c53c06dfda
.. _3: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=7e5b3c267d256822407a22fdce6afdf9cd13f9fb

See random.trust_cpu= in `The kernel’s command-line parameters`_


*randomize_kstack_offset*
-------------------------

Randomize offset of kernel stack::

    randomize_kstack_offset=1

See random.trust_cpu= in `The kernel’s command-line parameters`_


*slub_debug*
------------

Enable SLUB redzoning and sanity checking::

    slub_debug=ZF

See `Short users guide for SLUB`_


*slab_nomerge*
--------------

Do not merge slabs with similar size::

    slab_nomerge

See slab_nomerge in `The kernel’s command-line parameters`_


*vsyscall*
----------

Disable legacy *vsyscall* mechanism::

    vsyscall=none

See vsyscall= in `The kernel’s command-line parameters`_


Other Resources
===============

* `Kernel Self Protection Project`_ has recommendation for compile-time,
  boot-time and run-time parameters.


.. _hardening-runtime: https://packages.debian.org/stable/hardening-runtime
.. _security things in Linux v5.2: https://outflux.net/blog/archives/2019/07/17/security-things-in-linux-v5-2/
.. _security things in Linux v5.3: https://outflux.net/blog/archives/2019/11/14/security-things-in-linux-v5-3/
.. _Kernel Self Protection Project: https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project
.. _The kernel’s command-line parameters: https://docs.kernel.org/admin-guide/kernel-parameters.html
.. _Page Table Isolation (kernel docs): https://docs.kernel.org/x86/pti.html
.. _Short users guide for SLUB: https://docs.kernel.org/vm/slub.html
