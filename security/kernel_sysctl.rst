.. index:: hardening; sysctl, hardening; kernel

###########################
Kernel Hardening via Sysctl
###########################

Using ``sysctl``
================

Settings that can be modified via */proc/* filesystem or
:man:`sysctl(8)`.

Example sysctl config (``/etc/sysctl.d/50-custom.conf``)::

    kernel.dmesg_restrict = 1
    fs.protected_symlinks = 1

Load config::

    sysctl -p /etc/sysctl.d/50-custom.conf

.. tip::

    On Debian, the package `hardening-runtime`_ can be installed which
    configures many of this parameters for you. Look at
    ``/usr/lib/sysctl.d/10-hardening.conf`` after installing it.


Sysctl Parameters
=================

*abi.vsyscall32*
----------------

Disable VDSO for 32 bit syscalls::

    abi.vsyscall32 = 0

This setting is only meaningful on 64 bit architectures and
will likely break any 32 bit application. Luckily those no
longer really exist.

See:

* :man:`vdso(7)`
* `vDSO (kernel docs)`_
* `abi.vsyscall32 (kernel docs)`_


*dev.tty.ldisc_autoload*
------------------------

Disable automatic loading TTY line disciplines that rarely anyone
uses::

    dev.tty.ldisc_autoload = 0

See commit `7c0cca7c847e6e0`_ introducing the sysctl.


*fs.protected_fifos*
--------------------

Do not allow O_CREAT open on FIFOs in world-writable, sticky directories::

    fs.protected_fifos = 2

See `fs.protected_fifos (kernel docs)`_


*fs.protected_regular*
----------------------

Do not allow O_CREAT open on regular files in world-writable, sticky
directories::

    fs.protected_regular = 2

See `fs.protected_regular (kernel docs)`_


*fs.protected_symlinks*
-----------------------

Do not blindly follow symlinks in stick, world-writable directories
(like */tmp/*)::

    fs.protected_symlinks = 1

See `fs.protected_symlinks (kernel docs)`_


*fs.protected_hardlinks*
------------------------

Only allow creation of hardlinks when user has read/write access::

    fs.protected_hardlinks = 1

See `fs.protected_hardlinks (kernel docs)`_


*kernel.dmesg_restrict*
-----------------------

Restrict access to kernel logs via ``dmesg`` to super users (CAP_SYSLOG)::

    kernel.dmesg_restrict = 1

See `kernel.dmesg_restrict (kernel docs)`_


*kernel.kexec_load_disabled*
----------------------------

Do not allow a new kernel to be loaded via :man:`kexec(8)` in order
to skip hardware initialization during reboot::

    kernel.kexec_load_disabled = 1

See `kernel.kexec_load_disabled (kernel docs)`_


*kernel.kptr_restrict*
----------------------

Do not expose any kernel pointer via ``/proc/``::

    kernel.kptr_restrict = 2

See `kernel.kptr_restrict (kernel docs)`_


*kernel.unprivileged_bpf_disabled*
----------------------------------

Do not allow unprivileged users to load eBPF programs
(via :man:`bpf(2)`)::

    kernel.unprivileged_bpf_disabled = 1

See `kernel.unprivileged_bpf_disabled (kernel docs)`_


*kernel.yama.ptrace_scope*
--------------------------

Only allow admins to attach via ptrace::

    kernel.yama.ptrace_scope = 2

Only allow admins and parent processes to attach via ptrace::

    kernel.yama.ptrace_scope = 1

Disable ptrace attachment entirely (reboot required to undo)::

    kernel.yama.ptrace_scope = 3

See `kernel.yama.ptrace_scope (kernel docs)`_


*kernel.panic_on_oops*
----------------------

Trigger a kernel panic on BUG and oops::

    kernel.panic_on_oops = 1

This will make certain attacks harder as it will be harder to
corrupt the kernel. Drawback is that DoS attacks may be easier
to carry out as the kernel is more likely to panic.

Set `kernel.panic`_ also to force a reboot on panic.

See `kernel.panic_on_oops (kernel docs)`_


*kernel.unprivileged_userns_clone*
----------------------------------

Do not allow unprivileged user to create user namespaces::

    kernel.unprivileged_userns_clone = 0

.. note::

    This sysctl option was not accepted upstream but is carried
    by many distrubitons including Debian.

.. note::

    Some application use an unprivileged `user namespace`_ for sandboxing,
    including Firefox, and thus disabling this feature may weaken sandboxes.
    When unprivileged user namespaces aren't used they can be safely disabled.
    If they are used, consider restricting access by critical services using
    :ref:`systemd-RestrictNamespaces`\ =user.

See `unprivileged_userns_clone patch`_


*net.core.bpf_jit_harden*
-------------------------

Enable BPF JIT compiler hardening for all users::

    net.core.bpf_jit_harden = 2

See `net.core.bpf_jit_harden (kernel docs)`_


*user.max_user_namespaces*
--------------------------

Disable user namespaces entirely::

    user.max_user_namespaces = 0

User namespaces don't have the best `security track record
<https://security.stackexchange.com/a/209533>`_.

.. note::

   Some processes may relay on this feature for sandboxing. See
   also note on `kernel.unprivileged_userns_clone`_.

   This will also render the (rarely used) :ref:`systemd-PrivateUser`
   systemd directive ineffective.

See:

* `kernel.unprivileged_userns_clone`_ (disable unprivileged user
  namespaces only)
* `user.max_user_namespaces (kernel docs)`_


*kernel.perf_event_paranoid*
----------------------------

Disallow any unprivileged use of perf events::

    kernel.perf_event_paranoid = 3

.. note::

   Patch for support of value ``3`` for this setting has been
   declined upstream bun many distribution, including Debian,
   carry it.

See:

* `perf_event_paranoid patch`_ in Debian
* `kernel.perf_event_paranoid (kernel docs)`_


*vm.mmap_rnd_bits*
------------------

Increase entropy used to map memory randomly::

    vm.mmap_rnd_bits = 32

A higher value indicates higher entropy. Obtain the highest allowed
value like this::

    i=$(</proc/sys/vm/mmap_rnd_bits)
    while echo $((i+1)) >/proc/sys/vm/mmap_rnd_bits 2>/dev/null; do ((i++)); done;
    echo $i

See `vm.mmap_rnd_bits (kernel docs)`_


*vm.mmap_rnd_compat_bits*
-------------------------

Increase entropy used to map memory randomly in compat mode::

    vm.mmap_rnd_compat_bits = 16

A higher value indicates higher entropy. Obtain the highest allowed
value like this::

    i=$(</proc/sys/vm/mmap_rnd_compat_bits)
    while echo $((i+1)) >/proc/sys/vm/mmap_rnd_compat_bits 2>/dev/null; do ((i++)); done;
    echo $i

This is only useful on architectures with a compatibility mode. For instance, on x86_64
which has an x86 compatibility mode.

See `vm.mmap_rnd_compat_bits (kernel docs)`_


*vm.unprivileged_userfaultfd*
-----------------------------

Do not allow unprivileged users to handle page faults::

    vm.unprivileged_userfaultfd = 0

See `vm.unprivileged_userfaultfd (kernel docs)`_


Other Resources
===============

* `Kernel Self Protection Project`_ has recommendation for compile-time,
  boot-time and run-time parameters.


.. _7c0cca7c847e6e0: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=7c0cca7c847e6e019d67b7d793efbbe3b947d004
.. _abi.vsyscall32 (kernel docs): https://docs.kernel.org/admin-guide/sysctl/abi.html#vsyscall32-x86
.. _fs.protected_fifos (kernel docs): https://docs.kernel.org/admin-guide/sysctl/fs.html#protected-fifos
.. _fs.protected_hardlinks (kernel docs): https://docs.kernel.org/admin-guide/sysctl/fs.html#protected-hardlinks
.. _fs.protected_regular (kernel docs): https://docs.kernel.org/admin-guide/sysctl/fs.html#protected-regular
.. _fs.protected_symlinks (kernel docs): https://docs.kernel.org/admin-guide/sysctl/fs.html#protected-symlinks
.. _hardening-runtime: https://packages.debian.org/stable/hardening-runtime
.. _net.core.bpf_jit_harden (kernel docs): https://docs.kernel.org/admin-guide/sysctl/net.html#bpf-jit-harden
.. _kernel.dmesg_restrict (kernel docs): https://docs.kernel.org/admin-guide/sysctl/kernel.html#dmesg-restrict
.. _kernel.kexec_load_disabled (kernel docs): https://docs.kernel.org/admin-guide/sysctl/kernel.html#kexec-load-disabled
.. _kernel.panic: https://docs.kernel.org/admin-guide/sysctl/kernel.html#panic
.. _kernel.panic_on_oops (kernel docs): https://docs.kernel.org/admin-guide/sysctl/kernel.html#panic-on-oops
.. _kernel.perf_event_paranoid (kernel docs): https://docs.kernel.org/admin-guide/sysctl/kernel.html#perf-event-paranoid
.. _kernel.kptr_restrict (kernel docs): https://docs.kernel.org/admin-guide/sysctl/kernel.html#kptr-restrict
.. _kernel.unprivileged_bpf_disabled (kernel docs): https://docs.kernel.org/admin-guide/sysctl/kernel.html#unprivileged-bpf-disabled
.. _kernel.yama.ptrace_scope (kernel docs): https://docs.kernel.org/admin-guide/LSM/Yama.html#ptrace-scope
.. _perf_event_paranoid patch: https://sources.debian.org/patches/linux/5.16%7Erc8-1%7Eexp1/features/all/security-perf-allow-further-restriction-of-perf_event_open.patch/
.. _unprivileged_userns_clone patch: https://sources.debian.org/patches/linux/latest/debian/add-sysctl-to-disallow-unprivileged-CLONE_NEWUSER-by-default.patch/
.. _user.max_user_namespaces (kernel docs): https://docs.kernel.org/admin-guide/sysctl/user.html#max-user-namespaces
.. _user namespace: https://manpages.debian.org/user_namespaces(7)
.. _vDSO (kernel docs): https://docs.kernel.org/admin-guide/abi-stable.html#vdso
.. _vm.mmap_rnd_bits (kernel docs): https://docs.kernel.org/admin-guide/sysctl/vm.html#mmap-rnd-bits
.. _vm.mmap_rnd_compat_bits (kernel docs): https://docs.kernel.org/admin-guide/sysctl/vm.html#mmap-rnd-compat-bits
.. _vm.unprivileged_userfaultfd (kernel docs): https://docs.kernel.org/admin-guide/sysctl/vm.html#unprivileged-userfaultfd
.. _Kernel Self Protection Project: https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project
