.. index:: DNS

DNS
===

CAA Policy
----------

See :ref:`caa`


DANE / TLSA
-----------

See :ref:`dane`


.. todo::

   * DNSSEC

     - validating resolver
     - for domain
