TLS Certificates
################

.. index:: TLS; ACME, ACME

ACME - Certbot
==============

.. index:: ACME; Certbot Config

Configuration via */etc/letsencrypt/cli.ini*
--------------------------------------------

Any global --some-option can be used in `cli.ini`_ by
omitting the leading hyphens (--). To set an option
for a specific certificate set the option on the
respective config file in ``/etc/letsencrypt/renewal/``.

Useful defaults::

    email = person@example.com
    agree-tos = true

Change key type::

    key-type = ecdsa
    elliptic-curve = secp384r1


Alternative chains::

    preferred-chain = ISRG Root X1

Specifying *ISRG Root X1* will omit the expired root
certificate included by default to `support ancient
versions of Android`_. [#f1]_

See also:

* `RFC 8555`_ (ACME)
* `Certbot Documentation <https://certbot.eff.org/docs/>`_


.. index:: DNS; CAA, TLS; CAA
.. _caa:

CAA
===

The CAA DNS record type can be used to limit what CA can
issue a certificate for a domain or subdomain.

Send reports about unauthorized request to issue a certificate
to a specific mail address::

    example.com. IN CAA 0 iodef "mailto:hostmaster@example.com"

Not all CAs support this.

Allow only Let's Encrypt to issue certificates::

    example.com. IN CAA 0 issue "letsencrypt.org"

Only allow a certain validation method and specific accounts::

    ; Allow letsencrypt
    example.com. IN CAA 0 issue "letsencrypt.org; accounturi=https://acme-staging.api.letsencrypt.org/acme/reg/571142; validationmethods=http-01"
    example.com. IN CAA 0 issue "letsencrypt.org; accounturi=https://acme-v01.api.letsencrypt.org/acme/reg/7738050; validationmethods=http-01"

You usually want to specify a production and a staging
`account URI`_ to ensure --dry-run works.

Prohibit issuance of a certificate::

    ; non-wildcard
    example.com. IN CAA 0 issue ";"

    ; wildcard
    example.com. IN CAA 0 issuewild ";"

Override policy for subdomain::

    example.com. IN CAA 0 issue "letsencrypt.org"
    subdomain.example.com. IN CAA 0 issue "ca.example.org"

When issuing certificate for www.subdomain.example.com, a CAA
record is searched for at www.subdomain.example.com,
subdomain.example.com, and example.com, in this order. The
first policy found applies. [#f2]_

See also:

* `Certificate Authority Authorization (CAA)`_ at Let's Encrypt
* `RFC 6844`_ (CAA)
* `RFC 8657`_ (ACME-CAA)

.. warning::

    *CAA* and *CNAME* cannot coexist for a subdomain as CNAME
    will delegate *CAA* as well.

.. todo::

    explain Certificate Transparency


.. rubric:: Footnotes

.. [#f1] *ISRG Root X1* is an RSA certificate. Consider using
         *ISRG Root X2*, ECDSA, in the future. See `Certificates`_
.. [#f2] See `Where to put the record <https://letsencrypt.org/docs/caa/#where-to-put-the-record>`_


.. _support ancient versions of Android: https://letsencrypt.org/2020/12/21/extending-android-compatibility.html
.. _Certificates: https://letsencrypt.org/certificates/
.. _cli.ini: https://certbot.eff.org/docs/using.html#configuration-file
.. _account URI: https://letsencrypt.org/docs/account-id/
.. _Certificate Authority Authorization (CAA): https://letsencrypt.org/docs/caa/#where-to-put-the-record
.. _RFC 6844: https://tools.ietf.org/html/rfc6844
.. _RFC 8657: https://tools.ietf.org/html/rfc8657
.. _RFC 8555: https://tools.ietf.org/html/rfc8555
