Mail
####

.. index:: DANE

.. _dane:

DANE / TLSA
===========

Enforce DANE for Outgoing Mail
------------------------------

.. index::
   pair: DANE; Postfix
   single: DNSSEC; DANE
   seealso: DNS; DNSSEC

Postfix
^^^^^^^

.. important::

   The configured DNS servers (``/etc/resolv.conf``) must validate DNSSEC. You
   can verify this by checking if ``dig brokendnssec.net`` returns *SERVFAIL* and
   ``dig ch.`` contains a *AD* (Authenticated Data) flag.

.. note::

    ``systemd-resolved``:

        While DNSSEC validation is supported and validation status is available via
        D-BUS API, ``systemd-resolved`` does not expose validation status via AD flag
        on the stub resolver at 127.0.0.53.


#. Enforce DANE, edit /etc/postfix/main.cf::

    smtp_dns_support_level = dnssec
    smtp_tls_security_level = dane

#. Go to https://havedane.net/ and send mails to the printed test
   addresses.

#. You should see one mail to *@wrong.havedane.net* being stuck in the outgoing
   mail queue because of an invalid certificate::

       $ mailq
       -Queue ID-  --Size-- ----Arrival Time---- -Sender/Recipient-------
       0DC9F14EE0D7     845 Sat Jan  1 20:10:47  sender@arbitrary.ch
                                                     (Server certificate not trusted)
                                                e109ac729550b8f9@wrong.havedane.net

       -- 0 Kbytes in 1 Request.

See also `DANE TLS authentication`_

.. yes, this page is http only
.. _DANE TLS authentication: http://www.postfix.org/TLS_README.html#client_tls_dane

.. todo::

   Document:

   * IMAP
   * DKIM
   * MTA-STS
   * SPF
