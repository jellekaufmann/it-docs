#####
Users
#####

********************
List logged in users
********************

.. code-block:: sql

    SELECT
        s.pk,
        p.username,
        CASE
          WHEN s.ip IN ('83.150.12.146', '::fff:83.150.12.146') or s.ip like '2001:8e3:5396:c92:%'
          THEN 'tocco office'
          ELSE s.ip
        END AS ip,
        now() - s.timestamp as age
    FROM nice_session AS s LEFT OUTER JOIN nice_principal AS p ON s.fk_principal = p.pk
    WHERE s.timestamp > now() - interval '30m'
    ORDER BY s.timestamp desc;
