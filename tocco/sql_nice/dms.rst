###
DMS
###

***********************
List all domains in use
***********************

SQL
===

.. code-block:: sql

    SELECT DISTINCT substring(url from '^https?://([^/]+)') AS url
    FROM (
        SELECT unnest(regexp_split_to_array(alias, E'\n')) AS url
        FROM nice_domain
        WHERE alias <> ''
        UNION SELECT url
        FROM nice_domain
        WHERE url <> ''
    ) AS sub
    ORDER BY 1;


Sample output
=============

.. code::

    extranet.tocco.ch
    manual.tocco.ch
    tocco.ch
    www.tocco.ch
