***********************
OpenShift Code Snippets
***********************

*********************************************
List ``routes`` with Missing SSL Certificates
*********************************************

Current Project
===============

Command
-------

.. code-block:: bash

    oc get route -o json \
        | jq -r '.items[]
                  | (.metadata.annotations."kubernetes.io/tls-acme" == "false") as $acme_disabled
                  | .spec
                  | if
                        (.path//"" | startswith("/.well-known/acme-challenge/"))
                        or
                        (.tls | has("key"))
                    then
                        empty
                    else
                        if $acme_disabled then
                            .host + .path + " (acme disabled)"
                        else
                            .host + .path
                        end
                    end
                '


Sample Output
-------------

.. code::

    example.net
    docs.example.net (acme disabled)
    www.example.net

Routes with an explicit ``kubernetes.io/tls-acme = "false"`` annotation are marked **acme disabled**.


All Projects
============

Command
-------

.. code-block:: bash

    for n in $(oc projects -q); do
        oc -n "$n" get route -o json \
            | jq -r '.items[]
                      | (.metadata.annotations."kubernetes.io/tls-acme" == "false") as $acme_disabled
                      | .spec
                      | if
                            (.path//"" | startswith("/.well-known/acme-challenge/"))
                            or
                            (.tls | has("key"))
                        then
                            empty
                        else
                            if $acme_disabled then
                                .host + .path + " (acme disabled)"
                            else
                                .host + .path
                            end
                        end
                    ' \
            | awk -v prefix="$n" '$0=prefix": "$0'
    done


Sample Output
-------------

.. code::

    toco-nice-spv: spvo.ch
    toco-nice-spv: www.spvo.ch
    toco-nice-svz: svz.tocco.ch (acme disabled)
    toco-nice-tocco: manual.tocco.ch

Routes with an explicit ``kubernetes.io/tls-acme = "false"`` annotation are marked **acme disabled**.


*******************************************
List ``routes`` with ACME disabled or unset
*******************************************

List all routes with missing ``kubernetes.io/tls-acme`` as **unset** and routes with
``kubernetes.io/tls-acme = "false"`` as **disabled**.


Current Project
===============

Command
-------

.. code-block:: bash

    oc get route -o json \
        | jq -r '.items[]
                  | .spec.host as $host
                  | (.spec.path//"") as $path
                  | if $path|startswith("/.well-known/acme-challenge/") | not then
                       .metadata.annotations
                       | if has("kubernetes.io/tls-acme") then
                             if ."kubernetes.io/tls-acme" == "true" then
                                 empty
                             elif ."kubernetes.io/tls-acme" == "false" then
                                 $host + $path + ": disabled"
                             else
                                 $host + $path + ": unknown"
                             end
                         else
                             $host + $path + ": unset"
                         end
                    else
                        empty  # paths starts with "/.well-known/acme-challenge/"
                    end
                '

Sample Output
-------------

.. code::

    backoffice.tocco.ch: unset
    cockpit.tocco.ch: unset
    extranet.tocco.ch: unset
    manual.tocco.ch: disabled


All Projects
============

Command
-------

.. code-block:: bash

    for n in $(oc projects -q); do
        oc -n "$n" get route -o json \
            | jq -r '.items[]
                      | .spec.host as $host
                      | (.spec.path//"") as $path
                      | if $path|startswith("/.well-known/acme-challenge/") | not then
                           .metadata.annotations
                           | if has("kubernetes.io/tls-acme") then
                                 if ."kubernetes.io/tls-acme" == "true" then
                                     empty
                                 elif ."kubernetes.io/tls-acme" == "false" then
                                     $host + $path + ": disabled"
                                 else
                                     $host + $path + ": unknown"
                                 end
                             else
                                 $host + $path + ": unset"
                             end
                        else
                            empty  # path starts with "/.well-known/acme-challenge/"
                        end
                    ' \
            | awk -v prefix="$n" '$0=prefix": "$0'
    done


Sample Output
-------------

.. code::

    toco-nice-tocco: cockpit.tocco.ch: unset
    toco-nice-tocco: extranet.tocco.ch: unset
    toco-nice-tocco: manual.tocco.ch: disabled
    toco-nice-zewo: www.zewo.ch: unset
    toco-nice-zewo: zewo.ch: unset
