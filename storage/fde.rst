####################
Full Disk Encryption
####################

****
LUKS
****

Create LUKS Partition
=====================

.. note::

    **Sector Size**

    On drives with 4 KiB blocks, use ``--sector-size 4096`` with ``luksFormat`` in
    order to improve performance. Use ``blockdev --getpbsz ${DEVICE}`` to get
    physical block size.

    In authentication mode, ``--sector-size 4096`` can always be used as content
    is journalled and atomacy of sector writes is thus not required.

    **"No compatible PBKDF options"**

    This error message indicates that memory allocation failed. This happens
    on Qubes OS in particular as memory is allocated to VMs dynamically.
    Increase memory or use ``--pbkdf-memory ${MEMORY_in_KiB}``.

    Default value is shown at end of ``--help`` output.

    **Authenticated Mode - Caveats**

    * Support is experimental.

    * Expect writes to be extremely slow. (Data is written
      twice because of journal.)

    * Athentication is done per (4096 byte) sector. Rollback
      attacks on sectors are possible.

    * No support for re-encryption.

    * No support for resizing.

With AES accelerator, unauthenticated::

    cryptsetup luksFormat --pbkdf argon2id <device> [key-file]

Without AES accelerator, unauthenticated::

    cryptsetup luksFormat --pbkdf argon2id -c xchacha20,aes-adiantum-plain64 <device> [key-file]

With AES accelerator, authenticated::

    cryptsetup luksFormat --pbkdf argon2id --cipher aes-xts-plain64 --integrity hmac-sha256 --sector-size 4096 <device> [key-file]

.. note::

    This mode can be slow. On hardware with AES accelerator, it may still
    be better to use the algorithm specification below.

Without AES accelerator, authenticated::

    cryptsetup luksFormat --pbkdf argon2id --cipher chacha20-random --integrity poly1305 --sector-size 4096 <device> [key-file]


Enable Discard / TRIM
=====================

Enable discard-mode permanently by adding a flag in the LUKS header::

    cryptsetup open --persistent --allow-discards <device> <name>


Configure Encrypted Volume via ``crypttab``/``fstab``
=====================================================

.. hint::

    Ensure cryptsetup is installed::

        apt install cryptsetup


Mount LUKS Volume on Boot
-------------------------

Create key file::

    mkdir -p /etc/luks
    chmod 700 /etc/luks

    # create key (or copy existing key to /etc/luks/${NAME}.key)
    (umask 0077; dd if=/dev/urandom of=/etc/luks/${NAME}.key bs=512 count=1)

/etc/crypttab:

.. parsed-literal::

    # <target name>   <source device>    <key file>            <options>
    luks-\ **${NAME}**      **${SOURCE_DEVICE}**  /etc/luks/\ **${NAME}**\ .key  luks

See also `Important crypttab Options`_ below.

Activate device:

.. parsed-literal::

    cryptdisks_start luks-\ **${NAME}**


/etc/fstab:

.. parsed-literal::

    /dev/mapper/luks-\ **${NAME}**  **${MOUNT_TARGET}**   **${FS-auto}**  **${OPTIONS-defaults}**  0  1

See also `Important fstab Options`_ below.

Mount:

.. parsed-literal::

    mount **${MOUNT_TARGET}**


Mount Volatile ``/tmp`` on Boot
-------------------------------

/etc/crypttab:

.. parsed-literal::

    # <target name> <source device>     <key file>    <options>
    luks-tmp        **${DEVICE_OR_FILE}**   /dev/urandom  size=256,tmp=ext4

See also `Important crypttab Options`_ below.

Activate device::

    cryptdisks_start luks-tmp

/etc/fstab::

    /dev/mapper/luks-tmp  /tmp  ext4  noatime  0  0

See also `Important fstab Options`_ below.

.. warning::

    Mounting ``/tmp`` after boot may break running services as existing
    ``/tmp`` is shadowed.

Mount::

    mount /tmp


Volatile Swap
-------------

/etc/crypttab:

.. parsed-literal::

    # <target name> <source device>     <key file>    <options>
    luks-swap       **${DEVICE_OR_FILE}**   /dev/urandom  size=512,swap,cipher=aes-xts-plain64

See also `Important crypttab Options`_ below.

Activate device::

    cryptdisks_start luks-swap

/etc/fstab:

.. parsed-literal::

    /dev/mapper/luks-swap  none  swap  pri=10,nofail  0  0

See also `Important fstab Options`_ below.

Enable swap device::

    swapon /dev/mapper/luks-swap

Or enable all swap devices listed in ``/etc/fstab``::

    swapon -a


Important ``crypttab`` Options
==============================

================ =============================================================
 discard          Pass discard/TRIM commands through to underlying device.
 loop             Create loop device. Required if source device is a file
 luks             Expect source device to contain LUKS header.
 tmp=${FS}        Run mkfs to create filesystem of type ${FS}. **Irrevocably
                  destroys data on source device.**
================ =============================================================

See also :man:`crypttab(5)`.


Important ``fstab`` Options
===========================

============ ==================================================================
  discard     Issue discard/TRIM commands for freed disk areas.
  relatime    Only update access time when modification/change time is updated.
  nofail      Tell systemd to continue boot when mounting fails.
============ ==================================================================

See also :man:`fstab(5)`.
