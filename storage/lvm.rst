###
LVM
###

****************
Read/Write Cache
****************

Enable Cache
============


Create ${CACHE_LV}::

    lvcreate --type raid1 -m 1 -n ${CACHE_LV} ${VG} ${CACHE_PV1} ${CACHE_PV2}

Enable cache::

    lvconvert --type cache --cachepool ${CACHE_LV} --cachemode writeback --chunksize 256 ${VG}/${LV}

.. tip::

    * Only use ~75% of an SSD to ensure good performance.
    * Run TRIM on the empty space (:man:`blkdiscard(8)`).

Disable Cache
=============

.. code::

    lvconvert --splitcache ${VG}/${LV}


Run Raid Check
==============

.. code::

    lvchange --syncaction check ${VG}/${LV}

.. tip::

    You can use ``--maxrecoveryrate ${RATE}`` to reduce the impact on performance.

    Rate is per device. Default unit is *KiB/s/device*.


See Also
========

* :man:`lvmcache(7)`
* :man:`lvmraid(7)`
