####
Ext4
####

******************
Metadata Checksums
******************

Metadata checksums help to detect errors early. Hopefully, before
destroying the whole filesystem. It's enabled by default in *mkfs.ext4*
but there are still installers out there not enabling it or using the
32 bit version of ext4 with weaker checksums.

Test of checksums are enabled:

.. parsed-literal::

   $ dumpe2fs -h ${DEVICE} | grep features:
   Filesystem features:      has_journal ext_attr resize_inode dir_index filetype needs_recovery extent **64bit** flex_bg sparse_super large_file huge_file dir_nlink extra_isize **metadata_csum**
   Journal features:         journal_incompat_revoke journal_64bit journal_checksum_v3

If you see **64bit** and **metadata_csum**, checksums are enabled and the stronger
64 bit version is used.

Enabling metadata checksums on an offline device:

#. Clean the filesystem::

       e2fsck -fD ${DEVICE}

#. Convert FS to 64 bit::

       resize2fs -b ${DEVICE}

   64 bit version of ext4 uses stronger hash.

#. Enable checksums::

       tune2fs -O metadata_csum ${DEVICE}

See also:

* `Ext4 wiki <https://ext4.wiki.kernel.org/index.php/Ext4_Metadata_Checksums>`_
