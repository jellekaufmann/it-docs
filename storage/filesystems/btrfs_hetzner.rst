=====================================
Convert Hetzner Cloud Server to BTRFS
=====================================

Converts the default ext4 root FS that comes with Hetzner's Cloud Servers
to BTRFS. Tested with Debian 12 "bookworm".

#. (optional) Create a snapshot / backup

#. "Enable rescue & power cycle" in control panel

#. Login to host::

       ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@${HOST}

   For security reasons, do not -o StrictHostKeyChecking=no when using a password-based login.

#. Check ext4 filesystem::

       e2fsck -fvy /dev/sda1

#. Convert to BTRFS::

       btrfs-convert --csum xxhash /dev/sda1

#. (optional) Check FS::

       btrfsck /dev/sda1

#. Mount (and enable space_cache v2)::

       mount -o discard=async,space_cache=v2 /dev/sda1 /mnt

#. (optional) Check FS part 2::

       btrfs scrub start -B /dev/sda1

#. Create subvolume for *root*::

       btrfs subvolume create /mnt/@rootfs

#. Move data to new *root* subvolume::

       mv /mnt/* /mnt/@rootfs/

#. Make new root the default volume::

       btrfs subvolume set-default /mnt/@rootfs

#. Umount FS::

       umount /mnt

#. Remount with new default subvolume::

      mount -o discard=async,space_cache=v2 /dev/sda1 /mnt

   (Remounting to make sure Grub detects location of /boot properly.)

#. Update /etc/fstab::

       sed -r -i 's!UUID=.*\s/\s!# &!' etc/fstab
       echo "UUID=$(blkid -s UUID -o value /dev/sda1) / btrfs discard=async,noatime 0 1" >>etc/fstab

#. Mount various (virtual) filesystems::

       mount --bind /dev /mnt/dev \
         && mount --bind /sys /mnt/sys \
         && mount --bind /proc /mnt/proc \
         && mount /dev/sda15 /mnt/boot/efi/ \
         && mount --bind /sys/firmware/efi/efivars /mnt/sys/firmware/efi/efivars

#. Update boot loader::

       chroot /mnt update-grub
       chroot /mnt grub-install

#. Reboot::

       reboot

#. Login to host

#. (possibly with some delay) Clean up / optimize

   .. warning::

      Consider delaying this step to ensure BTRFS is working properly. After this cleanup,
      you can no longer easily revert back to ext4 using ``btrfs-convert --rollback``.

   .. code::

      btrfs subvolume delete /ext2_saved/
      btrfs balance start -m .
      btrfs balance start -mconvert=dup .

   .. note::V

      ``-mconvert-dup`` duplicates all metadata. Metadata is usually small and duplicating it
      uses little space but allow automatic repair of many serious corruptions.

#. (optional, possibly with some delay) use duplicate data

   .. tip::

      This duplicates all data, allowing data be repaired automatically from the duplicates
      as needed. You are generally better of investing in proper backups but it does not
      hurt to enable this when the server has enough spare space. Use ``-dconvert=single``
      to revert.

   .. warning::

      Warning from previous step applies.

   .. code::

          btrfs balance start -dconvert=dup .
