.. definitions included in all ReST files
.. ======================================

.. global roles
.. ------------

.. role:: strike
    :class: strike

.. role:: red
    :class: red


.. global links
.. ------------

.. _AppVM: https://www.qubes-os.org/getting-started/#appvms-qubes-and-templatevms
.. _Debian: https://www.debian.org
.. _Qubes OS: https://qubes-os.org
.. _TemplateVM: https://www.qubes-os.org/getting-started/#appvms-qubes-and-templatevms
.. _Tocco: https://www.tocco.ch

.. meta::
   :name=referrer: no-referrer
   :http-equiv=content-security-policy: default-src 'self'; img-src 'self' https://gitlab.com/pgerber/it-docs/; script-src 'self' 'sha256-aKkL5uJIn8IT6NRaVnTqZYwpUdS1WHm+y1JpAnirHI4='; style-src 'self' 'unsafe-inline'

.. raw:: html

    <div class="build-pipeline-status">
      <a href="https://gitlab.com/pgerber/it-docs/-/pipelines"
         referrerpolicy="no-referrer">
        <img alt=""
             src="https://gitlab.com/pgerber/it-docs/badges/master/pipeline.svg"
             referrerpolicy="no-referrer"
             height="20"
             width="116"/>
      </a>
    </div>
