.. index:: Raspberry PI; Install

RPi Install
###########

#. Get `Raspbian / Raspberry Pi OS`_

#. Copy image to SD card

#. Enable metadata checksums for ext4::

    e2fsck -fD /dev/${sdcard}2
    resize2fs -b /dev/${sdcard}2
    tune2fs -O metadata_csum /dev/${sdcard}2

  .. note::

      This enables 64bit mode which supports longer checksums.

#. Mount::

    mount /dev/${sdcard}2 /mnt

#. Enable SSHD::

    ln -s /lib/systemd/system/ssh.service /mnt/etc/systemd/system/sshd.service
    ln -s /lib/systemd/system/ssh.service /mnt/etc/systemd/system/multi-user.target.wants/ssh.service

.. note::

    It's possible use chroot to enable SSHD via systemctl but that
    only works if the binary format of the target is supported::

        systemctl --root /mnt enable ssh

#. Set temporary password for *pi*::

    sudo chpasswd -c SHA512 --root /mnt <<<"pi:${PASSWORD}"

#. Manage ethernet via *networkd*

   Create */mnt/etc/systemd/network/50-ether.network*:

   .. code-block:: ini

       [Match]
       Type=ether

       [Network]
       DHCP=yes

       [DHCP]
       SendHostname=false

#. Disable dhcpcd::

       rm /mnt/etc/rc2.d/S01dhcpcd

#. Enable networkd::

       systemctl --root /mnt enable systemd-networkd

#. Enabled resolved::

       systemctl --root /mnt enable systemd-resolved

#. Create random seed for `systemd-random-seed.service`_::

       install -m 600 -o root -g root /dev/null /mnt/var/lib/systemd/random-seed
       dd if=/dev/urandom of=/mnt/var/lib/systemd/random-seed bs=1 count=512

#. Umount::

    umount /mnt

#. Bring RPi online and connect via SSH

#. Remove dhcp client::

    apt remove dhcpcd5

.. _Raspbian / Raspberry Pi OS: https://www.raspberrypi.org/software/operating-systems/
.. _systemd-random-seed.service: https://www.freedesktop.org/software/systemd/man/systemd-random-seed.service.html
