###
Dev
###

Setup instruction for my ``dev``\ eloper VM.


.. index:: Qubes OS; Docker
.. _qubes-docker:

******
Docker
******

Start on AppVM Boot
===================

In TemplateVM:

.. code-block:: bash

    sudo systemctl mask docker
    sudo systemctl stop docker

In AppVM add this to ``/rw/config/rc.local``:

.. code-block:: bash

    systemctl umask docker
    systemctl start docker.socket


Persistent Storage
==================

add to ``/rw/config/qubes-bind-dirs.d/50_user.conf``:

.. code-block:: bash

    binds+=( '/var/lib/docker' )


.. index:: Git; Configuration

***
Git
***


Show Decoration in ``git log``
==============================

(show tags and branches)

.. code-block:: bash

    git config --global log.decorate auto


Configure Name and Mail
=======================

.. code-block:: bash

   git config --global user.name "Peter Gerber"
   git config --global user.email ${mail}


Configure GPG Signing
=====================

.. code-block:: bash

   git config --global user.signingkey=${fingerprint}

   # sign by default
   git config --global commit.gpgsign=true


Check Transfered Objects
========================

.. code-block:: bash

    git config --global fetch.fsckObjects true


Files Ignored by Default
========================

Create ``~/.config/git/ignore``:

.. code::

    *~
    .*.swp
