#####
Tocco
#####

This document describes how to set up an `AppVM`_ based on a `Debian`_ `TemplateVM`_ running on `Qubes OS`_
that contains all the tools necessary for developers and DevOps at `Tocco`_.


****************************
Step 1: Get and Extract IDEA
****************************

Download the `latest version <https://www.jetbrains.com/idea/download/>`__ and extract it:

.. code-block:: bash

    cd ~/Download
    wget https://download-cf.jetbrains.com/idea/ideaIC-XXXX.X.X.tar.gz
    tar xf ideaIC-XXXX.X.X.tar.gz
    ln -s ideaIC-XXXX.X.X idea
    mkdir -p ~/bin
    ln -s ../install/idea/bin/idea.sh ~/bin/idea

If an update is released that forces you to download a new \*.tar.gz file, extract it and replace the link
with ``ln -sfn ideaIC-XXXX.X.X idea``.


********************
Step 2: Install Java
********************

Install default JDK:

.. code-block:: bash

    apt install default-jdk

… or install a particular version:

.. code-block:: bash

    # install Java 8
    apt install openjdk-8-jdk

… or, if you need a newer version, get it from the backports repository:

.. code-block:: bash

    cat >>/etc/apt/source.list.d/backports <EOF
    deb http://ftp.debian.org/debian $(lsb_release -cs)-backports main
    EOF
    sudo apt update

    # install Java 9
    apt install openjdk-9-jdk


*********************
Step 3: Install Maven
*********************

.. code-block:: bash

    apt install maven

By default, the memory limit is based on available memory but that might not always be enough. (In Qubes the VM gets
memory assigned only when needed. Hence, this is needed.)

.. code-block:: bash

    echo 'export MAVEN_OPTS="-Xmx1024m"' >>~/.profile

.. important::

    Close and reopen your terminal for the changes to take effect.


***********************
Step 4: Configure Maven
***********************

.. code-block:: bash

    mkdir -p ~/.m2
    cat >~/.m2/settings.xml <<EOF
        THE ACTUAL CONTENT THAT GOES HERE CONTAINS A PASSWORD, GET IT AT:
        https://wiki.tocco.ch/wiki/index.php/Maven_2#settings.xml
    EOF


*****************
Step 5: Setup SSH
*****************

Create a key
============

.. code-block:: bash

    ssh-keygen -t rsa -b 4096
    cat ~/.ssh/id_rsa.pub # copy key and paste in the next step


Distribute Key
==============

Add key on https://git.tocco.ch (**Your name** (top right corner) → **Settings** → **SSH Public Keys** → **Add Key** …)

You also want to give the content of **~/.ssh/id_rsa.pub** to someone of operations if you want SSH access to any of the
servers (e.g. ask in the operations channel to have your key added).


Configure SSH
=============

.. code-block:: bash

    cd ~/src
    git clone https://github.com/tocco/tocco-dotfiles
    cat >>~/.ssh/config <<EOF
    # overwrite settings here, before the "Include"

    Host *
        Include ~/src/tocco-dotfiles/ssh/config
    EOF

*****************
Step 6: Setup Git
*****************

.. code-block:: bash

    sudo apt install git
    git config --global user.email "${USERNAME}@tocco.ch"  # replace ${USERNAME} with pgerber, …
    git config --global user.name "${YOUR_NAME}"  # replace ${YOUR_NAME} with "Peter Gerber", …

Ignore some files in all repository by default:

.. code-block:: bash

    cat >>~/.config/git/ignore <<EOF
    .idea/
    *~
    .*.swp
    EOF


***************************
Step 7: Get a copy of Nice2
***************************

.. code-block:: bash

    mkdir ~/src
    cd ~/src
    git clone ssh://${USERNAME}@git.tocco.ch:29418/nice2  # replace ${USERNAME} with pgerber, …
    cd nice2
    scp -p -P 29418 ${USERNAME}@git.tocco.ch:hooks/commit_msg hooks/  # replace ${USERNAME} with pgerber, …

Test if Maven build works:

.. code-block:: bash

    mvn -am install -T1.5C -DskipTests  # add "-Dmac" on Mac


**********************************
Step 8: IDEA: Create Desktop Entry
**********************************

.. note::

    This doesn't actually work in Qubes OS but may be useful in other setups.

open ``idea`` and find **Tools** → **Create Desktop Entry…**


**********************
Step 9: Configure IDEA
**********************

Increase memory available to IDEA
=================================

#. **Help** → **Edit Custom VM Options…**
#. Say **Yes** to create a config file
#. Set max. memory (-Xmx) to something sensible (e.g. -Xmx3096m)


Increase Memory IDEA Uses for Maven Import
==========================================

In Idea main window: **File** → **Settings** → **Build, Execution, Deployment** → **Build Tools** → **Maven** →
**Importing** → **VM options for Importer** → "**-Xmx1024m**" (Increase this even further if maven imports fails due to
out-of-memory.)


Increase Watch Limit
====================

.. code-block:: bash

    cat >>/rw/config/rc.local <<EOF
    sysctl fs.inotify.max_user_watches=524288
    EOF


*******************************
Step 10: Setup OpenShift Client
*******************************

Install Client
==============

Download the client tools from the `OpenShift download page <https://www.openshift.org/download.html>`__, extract it and
move the ``oc`` binary into ``$PATH``:

.. code-block:: bash

    cd ~/Download
    wget https://github.com/openshift/origin/releases/download/vX.X.X/openshift-origin-client-tools-vX.X.X-XXXXXXX-linux-64bit.tar.gz
    tar xf https://github.com/openshift/origin/releases/download/vX.X.X/openshift-origin-client-tools-vX.X.X-XXXXXXX-linux-64bit.tar.gz
    mkdir -p ~/bin
    cp openshift-origin-client-tools-vX.X.X-XXXXXXX-linux-64bit/oc ~/bin/oc


Enable Autocompletion
=====================

.. code-block:: bash

    cat >>~/.bashrc <<EOF
    eval $(oc completion bash)
    EOF


*********************
Step 11: Setup Docker
*********************

See :ref:`qubes-docker`
