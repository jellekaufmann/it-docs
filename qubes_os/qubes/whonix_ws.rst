##################
Whonix Workstation
##################

*******************
AppArmor Sandboxing
*******************

Install AppArmor profile::

    apt install apparmor-profile-torbrowser

Allow access to QubesIncoming, create ``etc/apparmor.d/local/home.tor-browser.firefox``::

    @{HOME}/QubesIncoming/ r,
    @{HOME}/QubesIncoming/** r,


***************************
Adjust Tor Browser Settings
***************************

Create ``/etc/torbrowser.d/user.js``::

    // Show Punycode in URL bar
    user_pref("network.IDN_show_punycode", true);

    // Set security level to safer
    user_pref("extensions.torbutton.inserted_security_level", true);
    user_pref("extensions.torbutton.security_slider", 2);

    // Enable Fission sandboxing
    user_pref("fission.autostart", true);

    // Switch to dark theme
    user_pref("browser.theme.toolbar-theme", 0);
    user_pref("extensions.activeThemeID", "firefox-compact-dark@mozilla.org");


**************
Alpha Releases
**************

Switch to alpha release of Tor Browser, create ``/etc/torbrowser.d/50_custom.conf``::

    tbb_download_alpha_version=true
