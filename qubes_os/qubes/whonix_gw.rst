##############
Whonix Gateway
##############

This is how I setup ``sys-whonix`` and ``sys-whonix-auth``. Apart from ``sys-whonix-auth`` containing
`Authenticated v3 Onion Services`_, both are identical.


********************************
Switching to Official Repository
********************************

Update to the more up-to-date Tor version from the upstream repository. See `Installing Tor on Debian`_ in the Tor's
official Documentation.

.. _Installing Tor on Debian: https://www.torproject.org/docs/debian.html.en


*************
Hardening Tor
*************

Enable hardening options:

.. code-block:: bash

    cat >>/usr/local/etc/torrc.d/50_user.conf <<EOF
    # enable seccomp-based sandbox
    Sandbox 1

    # Prohibit Tor to start processes (incompatible with transports)
    NoExec 1
    EOF

Install AppArmor profile for Tor Browser (Whonix workstation)::

    apt install apparmor-profile-torbrowser


******************************
Improving Connection Stability
******************************

Instruct Tor to build more reliable circuits for given ports:

.. code-block:: bash

    cat >>/usr/local/etc/torrc.d/50_user.conf <<EOF
    LongLivedPorts 21,22,706,1863,5050,5190,5222,5223,6523,6667,6697,8300,143
    EOF

The above list of ports contains the default set of long-lived ports (as of Tor 0.3.3) and port 143 (IMAP) in addition.


*************************************
Preventing Access to Private Networks
*************************************

Block access to private networks:

.. code-block:: bash

    host=…    # e.g. sys-whonix or sys-whonix-auth

    # remove existing rules
    while qvm-firewall "$host" del --rule-no 0 2>/dev/null; do :; done

    qvm-firewall "$host" add action=drop dsthost=10.0.0.0/8 comment="site local"
    qvm-firewall "$host" add action=drop dsthost=169.254.0.0/16 comment="site local"
    qvm-firewall "$host" add action=drop dsthost=172.16.0.0/12 comment="link local"
    qvm-firewall "$host" add action=drop dsthost=192.168.0.0/16 comment="site local"
    qvm-firewall "$host" add action=drop dsthost=fc00::/8 comment="site local"
    qvm-firewall "$host" add action=drop dsthost=fd00::/8 comment="unique site local"
    qvm-firewall "$host" add action=drop dsthost=fe80::/10 comment="link local"
    qvm-firewall "$host" add action=accept proto=tcp
    qvm-firewall "$host" add action=drop

.. warning::

    This will interfere with outgoing proxies in local networks.


.. _authenticated-v3-onion-services:

*******************************
Authenticated v3 Onion Services
*******************************

If you use authenticated onion services, consider setting up a separate NetVM
with access to authenticated hidden services.

Create one file per key using this naming scheme: ``/var/lib/tor/authdir/<name>.auth_private``.

Content::

    <onion-address>:descriptor:x25519:<base32-encoded-privkey>

Example::

    wa4yuhn2vloyc5nul6kbbzmyyzvom4bzjza4bdrarhrt5adfpetbqnid:descriptor:x25519:xbhzr3i7fombypfngvti4go4dymnyvxdxjh2lfkc4pwazsqwr3kq
