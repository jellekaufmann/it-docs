###
Net
###

.. index:: NetworkManager; Privacy Settings

*******************************
NetworkManager Privacy Settings
*******************************

#. Persist NetworkManager Config:

   Add this to ``/rw/config/qubes-bind-dirs.d/50_user.conf``::

       binds+=( '/etc/NetworkManager/conf.d/' )

#. Reboot

#. Add Custom NetworkManager Configuration:

   Add ``/etc/NetworkManager/conf.d/privacy.conf``

   .. code-block:: ini

       [device]
       wifi.scan-rand-mac-address=yes

       [connection]
       wifi.cloned-mac-address=random
       ethernet.cloned-mac-address=random

       # keep MAC only until reboot if MAC set to stable
       connection.stable-id=${CONNECTION}/${BOOT}

       # prefer temporary address
       ipv6.ip6-privacy=2

   .. attention::

       Not all drivers support setting a custom MAC address. It may be necessary to remove
       ``wifi.cloned-mac-address`` or ``ethernet.cloned-mac-address``.

   .. caution::

       NetworkManager doesn't allow disabling sending a hostname in DHCP requests globally (`Gnome Bug #768076`_). To
       ensure no hostname is sent, no static hostname must bo set (``hostnamectl set-hostname ''``).

#. Restart NetworkManager::

       sudo systemctl restart NetworkManager


.. _Gnome bug #768076: https://bugzilla.gnome.org/show_bug.cgi?id=768076
