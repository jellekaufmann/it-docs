#############
Wireguard VPN
#############

Install Wireguard in TemplateVM
*******************************

.. code-block::

       apt-get install -y wireguard

Create VM in *dom0*
*******************

#. Create and configure VM::

       qvm-create sys-wg-vpn --label black

       qvm-prefs sys-wg-vpn netvm sys-firewall
       qvm-prefs sys-wg-vpn provides_network true

       qvm-features service.disable-default-route 1
       qvm-features service.disable-dns-server 1
       qvm-features ipv6 1

#. Restrict internet access to VPN endpoint:

   .. parsed-literal::

          qvm-firewall sys-wg-vpn del --rule-no 0
          qvm-firewall sys-wg-vpn add dsthost=\ **<endpoint_ip>** proto=udp action=accept

Configure *sys-wg-vpn*
**********************

#. Persist configuration

   Add to ``/rw/config/qubes-bind-dirs.d/50_user.conf``::

       binds+=( '/etc/systemd/network' )

#. Reboot

#. Generate private key::

       (umask 077; wg genkey >/etc/systemd/network/wg0.private_key)
       chmod 640 /etc/systemd/network/wg0.private_key
       chgrp systemd-network /etc/systemd/network/wg0.private_key

#. Generate pre-shared key::

       (umask 077; wg genpsk >/etc/systemd/network/wg0.preshared_key)
       chmod 640 /etc/systemd/network/wg0.preshared_key
       chgrp systemd-network /etc/systemd/network/wg0.preshared_key

#. Print public key::

       wg pubkey </etc/systemd/network/wg0.private_key

#. Start systemd-networkd on boot

   Add to ``/rw/config/rc.local``::

       systemctl start systemd-networkd.service

#. Forward DNS requests

   ``/rw/config/qubes-firewall-user-script``:

   .. parsed-literal::

      resolver=\ **<resolver>**
      iptables -t nat -A PREROUTING -d 10.139.1.1/32 -p udp --dport 53 -j DNAT --to-destination "$resolver"
      iptables -t nat -A PREROUTING -d 10.139.1.1/32 -p tcp --dport 53 -j DNAT --to-destination "$resolver"
      iptables -t nat -A PREROUTING -d 10.139.1.2/32 -p udp --dport 53 -j DNAT --to-destination "$resolver"
      iptables -t nat -A PREROUTING -d 10.139.1.2/32 -p tcp --dport 53 -j DNAT --to-destination "$resolver"

      # Whonix filters fragmentation needed package
      iptables -t mangle -A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu


#. Have traffic to VPN endpoint bypass VPN:

   .. parsed-literal::

       ip route add **<endpoint_ip>** dev eth0

#. Configure Wireguard device

   Create ``/etc/systemd/networkd/wg.netdev``:

   .. parsed-literal::

      [NetDev]
      Name = wg0
      Kind = wireguard
      Description = Wireguard VPN

      [WireGuard]
      PrivateKeyFile = /etc/systemd/network/wg0.private_key

      [WireGuardPeer]
      Endpoint = **<endpoint_ip>**\ :\ **<endpoint_port>**
      AllowedIPs = 0.0.0.0/0, ::/0
      PublicKey = **<endpoint_public_key>**
      PresharedKeyFile = /etc/systemd/network/wg0.preshared_key


#. Configure VPN network

   Create ``/etc/systemd/networkd/wg.network``:

   .. parsed-literal::

       [Match]
       Name = wg0

       [Network]
       Address = **<vpn_ipv4>**
       Address = **<vpn_ipv6>**

       [Route]
       Gateway = **<vpn_gateway_ipv4>**
       Destination = 0.0.0.0/0

       [Route]
       Gateway = **<vpn_gateway_ipv6>**
       Destination = ::/0
