###
GPG
###

*************
Create GPG VM
*************

Install Required Utilities in TemplateVM
========================================

.. code::

    # needed for smartcard support
    apt install scdaemon pcscd pcsc-tools qubes-usb-proxy


Create VM
=========

.. code::

    qvm-create sys-gpg --label red --property netvm=


Adjust Authorization Validity
=============================

By default, authorizations are valid for 300 seconds, adjusted as needed:

.. code-block:: bash

    echo "export QUBES_AUTOACCEPT=3600" >>~/.profile


Always Require Confirmation
===========================

.. hint::

   GnuPG ask for confirmation by asking for the key's passphrase. Hence,
   **a passphrase must be set** for any confirmation to be printed.

Always require passphrase when signing (add to *~/.gnupg/gpg-agent.conf*)::

    ignore-cache-for-signing

    # Tell external programs not to cache the passphrases. This way we
    # get a confirmation dialog for every signature.
    no-allow-external-cache

Additionally, you can change how long passphrases are cached for a subkey
(add to *~/.gnupg/gpg-agent.conf*)::

    # remove passphrase from cache after key has not been used for N seconds
    default-cache-ttl 300

    # unconditionally remove passphrase from cache after N seconds
    max-cache-ttl 86400

If you have multiple keys, set a default key in ``~/.gnupg/gpg.conf``::

    default-key <16_digit_hex_fingerprint>


*************
Adjust Policy
*************

Write Policy
============

Default policy is ``ask`` which forces one to specify the target VM. To avoid this I updated the policy.

Allow any VM with a ``gpg-client`` tag to access the GPG VM ``sys-gpg``:

.. code-block:: bash

    cat >/etc/qubes/policy.d/30-custom-gpg.policy <<EOF
    # service name|*    +argument|*  source                  destination    action   [options]
    qubes.Gpg           *            @tag:gpg-client         sys-gpg        allow
    qubes.GpgImportKey  *            @tag:gpg-client         sys-gpg        ask      default_target=sys-gpg
    EOF


Tag VMs
=======

Now, tag all VMs that should have access the the GPG VM.

.. parsed-literal::

    qvm-tags **${ALLOWED_VM}** add gpg-client


********************
Configure Client VMs
********************

Set Target VM
=============


.. code-block:: bash

    cat >>~/.profile <<EOF
    export QUBES_GPG_DOMAIN=sys-gpg
    EOF

    echo sys-gpg >/rw/config/gpg-split-domain

Once this env. variable is set, the ``qubes-gpg-*`` commands should be available.


Setup Aliases
=============

.. code-block:: bash

    cat >>~/.bash_aliases <<EOF
    alias qgpg='qubes-gpg-client'
    alias qgpg-import='qubes-gpg-import-key'
    EOF


Configure Git
=============

.. code-block:: bash

    # set key for signing
    git config --global user.signingkey ${KEY_FPR}

    # override gpg command
    git config --global gpg.program qubes-gpg-client-wrapper

    # sign by default
    git config --global commit.gpgsign true


****************
Further Readings
****************

* `Qubes Split GPG <https://www.qubes-os.org/doc/split-gpg/>`_
* `RPC Policies <https://www.qubes-os.org/doc/rpc-policy/>`_
