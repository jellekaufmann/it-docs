.. index:: SSH; SSH Agent (Qubes VM), Qubes OS; SSH Agent VM

#########
SSH Agent
#########

This VM provides SSH agent access via ``qrexec``.

The example below uses this names, change as needed:

| SSH Agent VM: ``sys-ssh-agent``
| Client VM: ``admin``
| Tag to identify Client VMs: ``ssh-agent-client``
|


*********
Create VM
*********

In *dom0*::

    qvm-create --label black --property netvm= sys-ssh-agent

(Tested with Debian-based Template only.)


*******************
Prepare Template VM
*******************

Install dependencies::

    apt install ncat

Create ``/etc/qubes-rpc/custom.SshAgent`` …::

    #!/bin/sh

    notify-send "$(qubesdb-read /name): SSH agent access by $QREXEC_REMOTE_DOMAIN"

    exec 3>>~/ssh-agent.log
    flock -n 3
    echo "$(date --rfc-3339=ns): source: $QREXEC_REMOTE_DOMAIN" >&3
    exec 3>&-

    ncat -U $SSH_AUTH_SOCK

… and make it executable::

    chmod +x /etc/qubes-rpc/custom.SshAgent


************************
Setup RPC Policy in Dom0
************************

Create ``/etc/qubes/policy.d/30-custom-ssh-agent.policy``::

    # service name|*    +argument|*  source                  destination    action   [options]
    custom.SshAgent     *            @tag:ssh-agent-client   sys-ssh-agent  allow

Allow VM access (VM *admin* in this example)::

    qvm-tags admin add ssh-agent-client


.. index:: SSH; GnuPG SSH Agent

***************
In SSH Agent VM
***************

By default the OpenSSH ssh-agent is used. If you prefer to use GnuPG's agent, add this to ``~/.bashrc``::

    unset SSH_AGENT_PID
    export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"

Generate a key::

    ssh-keygen -t ed25519

Load SSH keys from default locations (OpenSSH only) by adding this to ``~/.profile``::

    ssh-add -q


*******************
Client VM (*admin*)
*******************

Create systemd service (``~/.config/systemd/user/remote-ssh-agent.service``):

.. code-block:: ini

   [Unit]
   Description=Connect to SSH agent on remote machine.

   [Service]
   UMask=177
   ExecStartPre=-/usr/bin/rm /var/run/user/%U/remote-ssh-agent.socket
   ExecStart=/usr/bin/ncat -k -l -U /var/run/user/%U/remote-ssh-agent.socket -c 'qrexec-client-vm sys-ssh-agent custom.SshAgent'

   [Install]
   WantedBy=default.target

Enable service::

    systemctl --user daemon-reload
    systemctl --user enable --now remote-ssh-agent

Add to ``~/.profile``::

    unset SSH_AGENT_PID
    export SSH_AUTH_SOCK=/var/run/user/$(id -u)/remote-ssh-agent.socket

In case you want to use the remote agent for Git only::

    git config --global core.sshCommand "SSH_AUTH_SOCK=/var/run/user/$(id -u)/remote-ssh-agent.socket SSH_AGENT_PID= ssh"
