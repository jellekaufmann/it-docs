##################
Dom0 Customization
##################

Copy/paste Key Combination
==========================

Use *super + c* for copying::

    qvm-features dom0 gui-default-secure-copy-sequence Mod4-c

Use *super + v* for pasting::

    qvm-features dom0 gui-default-secure-paste-sequence Mod4-v

The default is *ctrl + shift + c* and *crtl + shift + v* which are already
used in terminals and for raw pasting.


Number of VM Snapshots to Keep
==============================

A snapshot is created on every VM bootup. By default, the last three
are kept.

Increase default number of snapshots to five::

    qvm-pool set vm-pool -o revisions_to_keep=5

Show current configuration::

    qvm-pool info vm-pool

.. tip::

   **Change number of snapshots for root volume (/):**

   Only useful for TemplateVMs and StandaloneVMs. AppVMs
   use a volatile copy of a TemplateVM.

   Set::

       qvm-volume set <vm>:root revisons_to_keep <N>

   Show config::

       qvm-volume info <vm>:root

   **Change number of snapshots for private volume (/rw/):**

   On Qubes, ``/home/`` is also stored in ``/rw/``.

   Set::

       qvm-volume set <vm>:private revisons_to_keep <N>

   Show config::

       qvm-volume info <vm>:private


TRIM / Discard
==============

Pass discard through to physical disk:

#. Find LUKS device::

       dmsetup ls --target crypt

#. Enable discard::

       cryptsetup refresh --allow-discards <device>

Discard free space on :man:`lvremove(8)`:

#. Set in ``/etc/lvm/lvm.conf``::

       issue_discards = 1

See also:

* `Disk Trim (Qubes Community)`_


Kernel Parameters Dom0
======================

Add to ``/etc/default/grub``::

    GRUB_CMDLINE_LINUX="$GRUB_CMDLINE_LINUX random.trust_cpu=off"


Kernel Parameters Qubes
=======================


Set kernel parameters::

    qvm-prefs <vm> kernelopts 'random.trust_cpu=off vsyscall=none'

Set on template and AppVMs will inherit.

.. tip::

   List parameters for all VMs::

       qvm-ls --fields name,kernelopts


.. _Disk Trim (Qubes Community): https://github.com/Qubes-Community/Contents/blob/master/docs/configuration/disk-trim.md
