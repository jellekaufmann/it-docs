#######
Volumes
#######

************************************
Attach Additional, LVM-backed Volume
************************************

This can be useful when more storage is needed temporarily
or when certain directories should be excluded from backups.

#. Create *logical volume*::

       lvcreate -V ${SIZE} --thinpool vm-pool qubes_dom0 --name ${VOLUME_NAME}

   Set ``SIZE`` size to the virtual size of the disk (e.g. ``50GiB``).

#. Find device ID::

       $ qvm-block
       BACKEND:DEVID  DESCRIPTION               USED BY
       ${DEVICE_ID}   qubes_dom0-${VOLUME_NAME}

#. Attach to Qube::

       qvm-block attach ${QUBE_NAME} ${DEVICE_ID} -o frontend-dev=xvdl

   .. note::

      ``--persistent`` cannot be used as *${DEVICE_ID}* is not persistent
      across reboots.

BTRFS subvolumes offer an easy way to move multiple
directories onto an additional volume:

#. Format filesystem. See :ref:`btrfs-create-fs`.

#. Mount filesystem::

       mount /dev/xvdl /mnt

#. Create subvolumes for every directory::

       btrfs subvolume create /mnt/@volume1
       btrfs subvolume create /mnt/@volume2

#. Move files

#. Umount FS::

       umount /mnt

#. Mount individual volumes::

       mount /dev/xvdl ~user/volume1 -t btrfs -o discard=async,noatime,subvol=@volume1
       mount /dev/xvdl /var/lib/volume2 -t btrfs -o discard=async,noatime,subvol=@volume2


************************************
Resize Additional, LVM-backed Volume
************************************

#. (dom0) Resize volume by *${N}* GiB::

       lvresize --size +${N}GiB qubes_dom0/${VOLUME_NAME}

   Or resize volume to *${N}* GiB::

       lvresize --size ${N}GiB qubes_dom0/${VOLUME_NAME}

#. (Qube) Resize filesystem::

       btrfs filesystem resize max ${MOUNT_POINT}
