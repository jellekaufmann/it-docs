###############
Important Paths
###############

.. list-table:: Important Qubes-specific paths.
    :widths: 5 5 15
    :header-rows: 1

    * - Path
      - Location
      - Description

    * - _`/rw/config/qubes-bind-dirs.d/*.conf`
      - AppVM
      - **Persist file or directories across reboot.**

        Example, persist */path/to/file_or_dir*::

            binds+=('/path/to/file_or_dir')

        Files are copied to persistent storage located at */rw/bind-dirs/*
        and bind mounted.

        See also `How to make any file persistent (bind-dirs)`_

    * - /etc/qubes-bind-dirs.d/\*.conf
      - TemplateVM
      - **Persist file or directories across reboot.**

        See `/rw/config/qubes-bind-dirs.d/*.conf`_

    * - | /rw/config/qubes-firewall-user-script
        | /rw/config/qubes-firewall.d/
      - AppVM
      - **Custom firewall rules**

        Script executed during firewall configuration.

        .. important::

           This file is only used for a Qube that provides network
           to others. That is when, ``qvm-prefs <qube_name> provides_network``
           is true.

           On any other Qube, put firewall rules into `/rw/config/rc.local`_

        See also `Firewall`_

    * - _`/rw/config/rc.local`
      - AppVM
      - **Commands to be executed on (late) boot.**

    * - /usr/local/lib/systemd/system/
      - AppVM
      - **Systemd Units**

        May be used to define or override Systemd services, timer or
        other units.

        To create a service, create */usr/local/lib/systemd/system/<name>.service*.

        To override a service's settings, create */usr/local/lib/systemd/system/<name>.service/custom.conf*

        Note that */usr/local/* is persistent by default.

    * - /var/lib/tor/authdir/\*.auth_private
      - | :doc:`qubes/whonix_gw` AppVM
        | :doc:`qubes/onion` AppVM
      - **Tor .onion authorization files**

        See :ref:`authenticated-v3-onion-services`


.. _How to make any file persistent (bind-dirs): https://www.qubes-os.org/doc/bind-dirs/
.. _Firewall: https://www.qubes-os.org/doc/firewall
