#######
Generic
#######

BTRFS Support
=============

#. Grow FS via systemd on boot and disable access times:

   Add ``x-systemd.growfs`` and ``noatime`` to /etc/fstab.

   Example:

   .. parsed-literal::

    /dev/mapper/dmroot  /    ext4  defaults,discard,\ **noatime,x-systemd.growfs**  1  1
    /dev/xvdb           /rw  auto  noauto,defaults,discard,\ **noatime,x-systemd.growfs**    1  2

   Qubes has a built-in mechanism for resizing the FS on boot but it is limited to ext4.

   .. note::

      When converting to BTRFS, also replace ``ext4`` by ``btrfs``.


#. (Re)mount FS with discard=async:

   Create ``/etc/systemd/system/qubes-btrfs-remount@.service``:

   .. code-block:: ini

        [Unit]
        Description=Set custom mount options on BTRFS
        After=%i.mount
        Requisite=%i.mount
        ConditionKernelVersion=>=5.6

        [Service]
        ExecCondition=findmnt -t btrfs /%I
        ExecStart=mount -o remount,discard=async /%I

        [Install]
        WantedBy=multi-user.target

   Enable for /::

       systemctl enable qubes-btrfs-remount@-.service

   Enable for /rw/::

       systemctl enable qubes-btrfs-remount@rw.service
