######
Debian
######

Debian-specific template adjustments. Note that Whonix is Debian-based and some
customizations may work there as well.

Additional AppArmor Profiles
============================

.. code::

       apt install -y apparmor-profiles apparmor-profiles-extra


Vim as Default Editor
=====================

.. code::

       apt install -y vim
       update-alternatives --set editor /usr/bin/vim.nox


Enable APT Sandbox
==================

Create ``/etc/apt/apt.conf.d/90sandbox``::

    APT::Sandbox::Seccomp "true";


Allow tor+http:// URLs in ``sources.list``
==========================================

Create ``/etc/apt/apt.conf.d/00tor-proxy``::

    # Allow updates via sys-whonix and sys-onion. In
    # TemplateVMs this gets overridden by 01qubes-proxy.

    Acquire::tor::proxy "socks5h://10.152.152.10:9050/";

.. note::

   10.152.152.10 is the IP Whonix uses to contact Whonix GW from Whonix WS and
   for compatibility :doc:`the Onion Qube <../qubes/onion>` uses the same.
