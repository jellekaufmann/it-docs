#####
GnuPG
#####

***********
Tor Support
***********

Forward Tor SOCKS Requests
==========================

GnuPG tries to connect to ``socks5://localhost:9050`` when Tor is detected or ``--use-tor`` is used. Since Tor isn't
running on localhost, ``socat`` is used to forward the request to Tor running in the Whonix gateway.

#. Create a systemd service to forward requests

   Create  ``~/.config/systemd/user/tor-localhost-forward.service``:

   .. code-block:: ini

       [Unit]
       Description=Forward Tor request on localhost to Whonix

       [Service]
       ExecStart=/usr/bin/socat TCP-LISTEN:9050,bind=localhost,fork TCP:10.152.152.10:9102

       [Install]
       WantedBy=default.target

#. Reload ``systemd``::

       systemctl --user daemon-reload

#. Enable and start service::

       systemctl --user enable --now tor-localhost-forward.service


Configure ``gpg``/``dirmngr``
=============================

Config at ``~/.gnupg/dirmngr.conf``::

    use-tor
    keyserver hkps://keyserver.ubuntu.com
