########
Firewall
########

**********
List Rules
**********

``qvm-firewall`` Utility
========================

List rules of VM:

.. parsed-literal::

    $ qvm-firewall **<vm>**
    NO  ACTION  HOST            PROTOCOL  PORT(S)  SPECIAL TARGET  ICMP TYPE  COMMENT
    0   drop    10.0.0.0/8      -         -        -               -          private address
    1   drop    169.254.0.0/16  -         -        -               -          private address
    2   drop    172.16.0.0/12   -         -        -               -          link local
    3   drop    192.168.0.0/16  -         -        -               -          private address
    4   drop    fc00::/8        -         -        -               -          site local
    5   drop    fd00::/8        -         -        -               -          unique site local
    6   drop    fe80::/10       -         -        -               -          link local
    7   accept  -               tcp       -        -               -          -
    8   drop    -               -         -        -               -          -



**************
Changing Rules
**************

Available Rules
===============

===============  ===================================  =================================================================
Name              Example(s)                           Description
---------------  -----------------------------------  -----------------------------------------------------------------
 action           ``accept`` or ``drop``               Drop or accept packages matching entry
 comment          ``free text``                        Comment added to the entry
 dsthost          ``158.45.56.88``, ``10.0.0.0/8``     IPv4, IPv6, CIDR
 dstports         ``443-443``, ``1200-1400``           Range of ports
 proto            ``icmp``, ``tcp``, ``udp``           Network protocol
 specialtarget    ``dns``                              Special targets. ``dns`` matches the DNS servers defined for a
                                                       VM.
===============  ===================================  =================================================================

********
Examples
********

Disallow local networks:

.. code-block:: bash

    # remove existing rules
    qvm-firewall <vm> reset
    qvm-firewall <vm> del --rule-no 0      # remove default accept rule

    # add custom rules
    qvm-firewall <vm> add action=drop dsthost=10.0.0.0/8 comment="private address"
    qvm-firewall <vm> add action=drop dsthost=169.254.0.0/16 comment="private address"
    qvm-firewall <vm> add action=drop dsthost=172.16.0.0/12 comment="link local"
    qvm-firewall <vm> add action=drop dsthost=192.168.0.0/16 comment="private address"
    qvm-firewall <vm> add action=drop dsthost=fc00::/8 comment="site local"
    qvm-firewall <vm> add action=drop dsthost=fd00::/8 comment="unique site local"
    qvm-firewall <vm> add action=drop dsthost=fe80::/10 comment="link local"
    qvm-firewall <vm> add action=accept

Only allow connections to given IPs using UDP:

.. code-block:: bash

    # remove existing rules
    qvm-firewall <vm> reset
    qvm-firewall <vm> del --rule-no 0      # remove default accept rule

    qvm-firewall <vm> add dsthost=1234:abcd::1/128 proto=udp action=accept
    qvm-firewall <vm> add dsthost=1.2.3.4 proto=udp action=accept
