.. index:: systemd; automatic login

Automatically login on TTY
##########################

create file ``/etc/systemd/system/getty@.service.d/autologin.conf``:

.. code-block:: ini

    [Service]
    ExecStart=
    ExecStart=-/sbin/agetty --noclear --autologin user %I $TERM
