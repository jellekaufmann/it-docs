***
SSH
***

.. index:: SSH; SSHFS, Filesystem; SSHFS

Mount Remote Directory via SSH(FS)
==================================

Mount
-----

.. code::

    $ sshfs [user@]host:[dir] mountpoint -o reconnect,uid=$(id -u),gid=$(id -g),max_conns=8,ServerAliveInterval=60

Important options:

======================= =======================================================================
 compression=yes         Compress data during transfer. Only use this on slow connections
                         transferring easily compressible data.

 gid                     Set group of file.

 imapd=user              Map UID and GID of all files to the current user.

 max_conns=N             Open up to *N* parallel SSH connections. Improves responsiveness as
                         the underlying SFTP protocol is blocking.

                         **Support added in 3.7.0 released on 2020-01-03.**

 reconnect               Automatically reconnect if connection is lost. (Files need to be
                         re\ **open()**\ ed after reconnecting.)

                         Enable *ServerAliveInterval* to ensure reconnecting works properly.

 ServerAliveInterval=N   Terminate connection after 3 * *N* seconds. See
                         :man:`ServerAliveInterval <ssh_config(5)#ServerAliveInterval>` and
                         :man:`ServerAliveCountMax <ssh_config(5)#ServerAliveCountMax>` in
                         *ssh_config(5)*.

 uid                     Set owner of file.
======================= =======================================================================


Umount
------

Reglar umount::

    $ fusermount -u mountpoint

Lazy umount (i.e. after connection loss)::

    $ fusermount -uz mountpoint

See
---

* :man:`sshfs(1)`
* :man:`fusermount(1)`
* :man:`ssh_config(5)`
